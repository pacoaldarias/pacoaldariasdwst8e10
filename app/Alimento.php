<?php

class Alimento {

    private $id;
    private $nombre;
    private $energia;
    private $proteina;
    private $hidratocarbono;
    private $fibra;
    private $grasatotal;

    public function __construct($id, $nombre, $energia, $proteina, $hidratocarbono, $fibra, $grasatotal) {
        $this->id = $id;
        $this->nombre = $nombre;
        $this->energia = $energia;
        $this->proteina = $proteina;
        $this->hidratocarbono = $hidratocarbono;
        $this->fibra = $fibra;
        $this->grasatotal = $grasatotal;
    }

    public function getId() {
        return $this->id;
    }

    public function getNombre() {
        return $this->nombre;
    }

    public function getEnergia() {
        return $this->energia;
    }

    public function getProteina() {
        return $this->proteina;
    }

    public function getHidratocarbono() {
        return $this->hidratocarbono;
    }

    public function getFibra() {
        return $this->fibra;
    }

    public function getGrasatotal() {
        return $this->grasatotal;
    }

}

?>