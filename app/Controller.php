<?php

error_reporting(E_ALL);
ini_set('display_errors', '1');

class Controller {

    public function inicio() {
        $params = array(
            'mensaje' => 'Bienvenido al repositorio de alimentos',
            'fecha' => date('d-m-y'),
        );
        require __DIR__ . '/templates/inicio.php';
    }

    public function listar() {


        //Al crear el objeto, conectamos con la BD con los parámetros de config.php
        $m = new Model(Config::$mvc_bd_nombre, Config::$mvc_bd_usuario, Config::$mvc_bd_clave, Config::$mvc_bd_hostname);

        //Llamamos al método dameAlimentos del modelo y cargaremos los resultados en el array $params
        $params = array('alimentos' => $m->dameAlimentos(),);

        require __DIR__ . '/templates/mostrarAlimentos.php';
    }

    public function insertar() {
        $params = array(
            'nombre' => '',
            'energia' => '',
            'proteina' => '',
            'hc' => '',
            'fibra' => '',
            'grasa' => '',
        );

        $m = new Model(Config::$mvc_bd_nombre, Config::$mvc_bd_usuario, Config::$mvc_bd_clave, Config::$mvc_bd_hostname);

        //echo "Post insert " . $_POST['insertar'];

        if (isset($_POST['insertar'])) {

            // comprobar campos formulario
            if ($m->validarDatos($_POST['nombre'], $_POST['energia'], $_POST['proteina'], $_POST['hc'], $_POST['fibra'], $_POST['grasa'])) {
                if ($m->insertarAlimento($_POST['nombre'], $_POST['energia'], $_POST['proteina'], $_POST['hc'], $_POST['fibra'], $_POST['grasa'])) {
                    header('Location: index.php?ctl=listar');
                } else {

                    $params = array(
                        'nombre' => $_POST['nombre'],
                        'energia' => $_POST['energia'],
                        'proteina' => $_POST['proteina'],
                        'hc' => $_POST['hc'],
                        'fibra' => $_POST['fibra'],
                        'grasa' => $_POST['grasa'],
                    );
                    $params['mensaje'] = 'No se ha podido insertar el alimento. Revisa el formulario';
                }
            } else {
                $params = array(
                    'nombre' => $_POST['nombre'],
                    'energia' => $_POST['energia'],
                    'proteina' => $_POST['proteina'],
                    'hc' => $_POST['hc'],
                    'fibra' => $_POST['fibra'],
                    'grasa' => $_POST['grasa'],
                );
                $params['mensaje'] = 'Hay datos que no son correctos. Revisa el formulario';
            }
        }

        require __DIR__ . '/templates/formInsertar.php';
    }

    public function buscarPorNombre() {
        $params = array(
            'nombre' => '',
            'resultado' => array(),
        );

        $m = new Model(Config::$mvc_bd_nombre, Config::$mvc_bd_usuario, Config::$mvc_bd_clave, Config::$mvc_bd_hostname);

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $params['nombre'] = $_POST['nombre'];
            $params['resultado'] = $m->buscarAlimentosPorNombre($_POST['nombre']);
        }

        require __DIR__ . '/templates/buscarPorNombre.php';
    }

    public function ver() {
        if (!isset($_GET['id'])) {
            $params = array(
                'mensaje' => 'No has seleccionado ningun elemento que mostrar',
                'fecha' => date('d-m-y'),
            );
            require __DIR__ . '/templates/inicio.php';
        }

        $id = $_GET['id'];

        $m = new Model(Config::$mvc_bd_nombre, Config::$mvc_bd_usuario, Config::$mvc_bd_clave, Config::$mvc_bd_hostname);

        $alimento = $m->dameAlimento($id);

        $params = $alimento;
        //Si la consulta no ha devuelto resultados volvemos a la página de inicio
        if (empty($params)) {
            $params = array(
                'mensaje' => 'No hay alimento que mostar',
                'fecha' => date('d-m-y'),
            );
            require __DIR__ . '/templates/inicio.php';
        } else
            require __DIR__ . '/templates/verAlimento.php';
    }

}

?>
