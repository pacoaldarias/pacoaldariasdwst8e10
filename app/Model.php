<?php

//app/Model.php
include_once("Alimento.php");

class Model {

    protected $conexion;

    public function __construct($dbname, $dbuser, $dbpass, $dbhost) {


        $this->conexion = NULL;

        try {

            $bdconexion = new PDO('mysql:host=' . $dbhost . ';dbname='
                    . $dbname . ';charset=utf8', $dbuser, $dbpass);

            $this->conexion = $bdconexion;
        } catch (PDOException $e) {

            echo 'ERROR: ' . $e->getMessage();
        }
    }

    public function dameAlimentos() {


        $consulta = "select * from alimentos order by energia desc";
        //echo $consulta;
        $result = $this->conexion->query($consulta);
        $alimentos = array();
        $cont = 0;

        $filas = $result->fetchAll(PDO::FETCH_OBJ);
        //print_r($filas);
        foreach ($filas as $fila) {
            //echo $fila->id . "<br>";
            $alimento = new Alimento($fila->id, $fila->nombre, $fila->energia, $fila->proteina, $fila->hidratocarbono, $fila->fibra, $fila->grasatotal);
            $alimentos [$cont] = $alimento;
            $cont++;
        }

        $conexion = false;
        return $alimentos;
    }

    public function buscarAlimentosPorNombre($nombre) {


        $nombre = htmlspecialchars($nombre);

        $sql = "select * from alimentos where nombre like '" . $nombre .
                "' order by energia desc";
        $result = $this->conexion->query($sql);
        $alimentos = array();
        $cont = 0;

        $filas = $result->fetchAll(PDO::FETCH_OBJ);
        //print_r($filas);
        foreach ($filas as $fila) {
            //echo $fila->id . "<br>";
            $alimento = new Alimento($fila->id, $fila->nombre, $fila->energia, $fila->proteina, $fila->hidratocarbono, $fila->fibra, $fila->grasatotal);
            $alimentos [$cont] = $alimento;
            $cont++;
        }

        $conexion = false;
        return $alimentos;
    }

    public function dameAlimento($id) {

        $consulta = 'SELECT * FROM alimentos where id=:id;';
        //$consulta = 'SELECT * FROM profesor where id="' . $profesor_->getId() . '";';
        //$result = $this->conexion->query($consulta);

        $result = $this->conexion->prepare($consulta);
        $result->execute(array(":id" => $id));

        $profesores = array();
        $cont = 0;

        $fila = $result->fetch(PDO::FETCH_OBJ);
        $alimento = new Alimento($fila->id, $fila->nombre, $fila->energia, $fila->proteina, $fila->hidratocarbono, $fila->fibra, $fila->grasatotal);
        $conexion = false;

        return $alimento;
    }

    public function insertarAlimento($n, $e, $p, $hc, $f, $g) {


        try {
            $sql = "insert into alimentos (nombre, energia, proteina, "
                    . "hidratocarbono, fibra, grasatotal) values ('" .
                    $n . "'," . $e . "," . $p . "," . $hc . "," . $f . ","
                    . $g . ")";

            $result = $this->conexion->prepare($sql);

            if (!$result) {
                //echo "\nPDO::errorInfo():<br>";
                //print_r($this->conexion->errorInfo());
            }

            $count = $result->execute(array(
                ":n" => $n
                , ":e" => $e
                , ":p" => $p
                , ":hc" => $hc
                , ":f" => $f
                , ":g" => $g
            ));

            //echo $consulta . "<br\>";
            //echo "Count: " . $count . "<br\>";
            //print_r($result);
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
            return false;
        }

        $conexion = false;
        if ($count == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function validarDatos($n, $e, $p, $hc, $f, $g) {


        $valido = is_string($n) &
                is_numeric($e) &
                is_numeric($p) &
                is_numeric($hc) &
                is_numeric($f) &
                is_numeric($g);


        return ($valido);
    }

}

?>
